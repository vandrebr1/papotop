package br.com.vandre.papotop.atividades;

import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.app.AppCompatActivity;
import br.com.vandre.papotop.R;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private TextView tvLoginDescricao;
    private FirebaseUser usuarioAtual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciar();
    }

    private void declarar() {
        mAuth = FirebaseAuth.getInstance();
        tvLoginDescricao = findViewById(R.id.main_tvLoginDescricao);

    }

    private void iniciar() {
        declarar();
        usuarioAtual = mAuth.getCurrentUser();

        if (usuarioAtual == null) {
            finish();
        }

        tvLoginDescricao.setText(usuarioAtual.getDisplayName());

    }

}
