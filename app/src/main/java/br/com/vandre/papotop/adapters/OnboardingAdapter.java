package br.com.vandre.papotop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import br.com.vandre.papotop.R;
import br.com.vandre.papotop.model.ItemOnboarding;

public class OnboardingAdapter extends PagerAdapter {

    Context mContext;
    List<ItemOnboarding> mItemOnboardings;

    public OnboardingAdapter(Context mContext, List<ItemOnboarding> mItemOnboardings) {
        this.mContext = mContext;
        this.mItemOnboardings = mItemOnboardings;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemOnboarding = inflater.inflate(R.layout.item_onboarding, null);

        TextView descricao = itemOnboarding.findViewById(R.id.item_onboarding_tvDescricao);
        ImageView img = itemOnboarding.findViewById(R.id.item_onboarding_ivImagem);

        descricao.setText(mItemOnboardings.get(position).getDescricao());
        img.setImageResource(mItemOnboardings.get(position).getImg());

        container.addView(itemOnboarding);

        return itemOnboarding;
    }

    @Override
    public int getCount() {
        return mItemOnboardings.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
