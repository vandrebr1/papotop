package br.com.vandre.papotop.model;

public class ItemOnboarding {

    String descricao;
    int img;

    public ItemOnboarding(String descricao, int img) {
        this.descricao = descricao;
        this.img = img;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
