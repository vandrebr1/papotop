package br.com.vandre.papotop.atividades;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import br.com.vandre.papotop.R;
import br.com.vandre.papotop.adapters.OnboardingAdapter;
import br.com.vandre.papotop.app.Constantes;
import br.com.vandre.papotop.model.ItemOnboarding;

public class OnBoardingActivity extends AppCompatActivity {


    private ViewPager mViewPager;
    private OnboardingAdapter mOnboardingAdapter;
    private TabLayout mTabLayout;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        if (restorePrefData()) {
            finish();
        } else {
            savePrefsData();
        }

        iniciar();
    }

    private void declarar() {

        mTabLayout = findViewById(R.id.onboarding_tbTabLayout);
        mViewPager = findViewById(R.id.onboarding_vpPager);
        btnLogin = findViewById(R.id.onboarding_btnLogin);

    }

    private void iniciar() {
        declarar();

        final List<ItemOnboarding> mList = new ArrayList<>();
        mList.add(new ItemOnboarding(getString(R.string.slide1), R.drawable.slide1));
        mList.add(new ItemOnboarding(getString(R.string.slide2), R.drawable.slide2));
        mList.add(new ItemOnboarding(getString(R.string.slide3), R.drawable.slide3));
        mList.add(new ItemOnboarding(getString(R.string.slide4), R.drawable.slide4));

        mOnboardingAdapter = new OnboardingAdapter(this, mList);
        mViewPager.setAdapter(mOnboardingAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == mList.size() - 1) {
                    carregouUltimaPagina();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean restorePrefData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constantes.PAPOTOP_PREFS, MODE_PRIVATE);
        return pref.getBoolean(Constantes.ONBOARDING_OK, false);
    }

    private void savePrefsData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constantes.PAPOTOP_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Constantes.ONBOARDING_OK, true);
        editor.apply();
    }

    private void carregouUltimaPagina() {
        //mTabLayout.setVisibility(View.INVISIBLE);
    }

}